package main

import (
	"bitbucket.org/jmcoello/bbm-steganography/engines"
	"flag"
	"os"
)

func main () {
	var base, message, out, in, engine string
	flag.StringVar(&base, "base", "", "input base file")
	flag.StringVar(&message, "message", "", "input message file")
	flag.StringVar(&out, "out", "", "output file")
	flag.StringVar(&in, "in", "", "input file")
	flag.StringVar(&engine, "engine", "auto", "engine")

	flag.Parse()

	var steganographyEngine engines.SteganographyEngine

	switch engine {
	default:
		steganographyEngine = &engines.BMPSteganographyEngine{}
	}

	outFile, err := os.OpenFile(out, os.O_CREATE|os.O_WRONLY, 0755)
	defer outFile.Close()
	if err != nil {
		panic(err)
	}

	if len(in) > 0 {
		inFile, err := os.OpenFile(in, os.O_CREATE|os.O_RDONLY, 0755)
		defer inFile.Close()
		if err != nil {
			panic(err)
		}

		err = steganographyEngine.Reveal(inFile, outFile)
		if err != nil {
			panic(err)
		}
		os.Exit(0)
	}

	if len(base) > 0 && len(message) > 0 {
		baseFile, err := os.OpenFile(base, os.O_CREATE|os.O_RDONLY, 0755)
		defer baseFile.Close()
		if err != nil {
			panic(err)
		}

		messageFile, err := os.OpenFile(message, os.O_CREATE|os.O_RDONLY, 0755)
		defer baseFile.Close()
		if err != nil {
			panic(err)
		}

		err = steganographyEngine.Hide(baseFile, messageFile, outFile)
		if err != nil {
			panic(err)
		}
		os.Exit(0)
	}

	panic("Bad Args")

}