package engines

import "io"

type SteganographyEngine interface {
	Hide(base io.Reader, message io.Reader, out io.Writer) (err error)
	Reveal(in io.Reader, out io.Writer) (err error)
}
