package engines

import (
	"errors"
	"golang.org/x/image/bmp"
	"image"
	"image/color"
	"io"
	"io/ioutil"
)

type BMPSteganographyEngine struct {}

/*
 makeZero takes the 4 bits less significant and make them 0
 */

func makeZero (n uint32) (uint32) {
	return n & 0xFFF0
}

/*
 makeOdd takes the bit less significant and make it 0
 */

func makeOdd (n uint32) (uint32) {
	return n | 0x0001
}

/*
 makeOdd takes the bit less significant and make it 1
 */

func makeEven (n uint32) (uint32) {
	return n & 0xFFFE
}

func convertColor (c color.Color, rm bool, gm bool, bm bool, eof bool) (color.Color) {
	r, g, b, _ := c.RGBA()

	if rm {
		r = makeEven(r)
	} else {
		r = makeOdd(r)
	}

	if gm {
		g = makeEven(g)
	} else {
		g = makeOdd(g)
	}

	if bm {
		b = makeEven(b)
	} else {
		b = makeOdd(b)
	}

	if eof {
		b = makeZero(b)
	}

	return color.RGBA{
		R: uint8(r),
		G: uint8(g),
		B: uint8(b),
		A: uint8(0xFF),
	}
}

type colorIterator struct {
	x int
	y int
	maxX int
	maxY int
}

func (i *colorIterator) Next() (x int, y int, err error) {
	i.x++
	if i.x > i.maxX-1 {
		i.x = 0
		i.y++
	}
	if i.y > i.maxY-1 {
		return 0, 0, errors.New("EOF")
	}
	return i.x, i.y, nil
}

func (e BMPSteganographyEngine) Hide (base io.Reader, message io.Reader, out io.Writer) (err error) {

	im, err := bmp.Decode(base)
	if err != nil {
		return
	}

	bytes, err := ioutil.ReadAll(message)
	if err != nil {
		return
	}

	var x, y int
	colorIterator := colorIterator{
		0,
		0,
		im.Bounds().Max.X,
		im.Bounds().Max.Y,
	}

	for i, b := range bytes {

		c := im.At(x, y)
		c = convertColor(c, b & 0x80 > 0, b & 0x40 > 0, b & 0x20 > 0, false)
		im.(*image.RGBA).Set(x, y, c)

		x, y, err = colorIterator.Next()
		if err != nil {
			return err
		}

		c = im.At(x, y)
		c = convertColor(c, b & 0x10 > 0, b & 0x08 > 0, b & 0x04 > 0, false)
		im.(*image.RGBA).Set(x, y, c)

		x, y, err = colorIterator.Next()
		if err != nil {
			return err
		}

		c = im.At(x, y)
		c = convertColor(c, b & 0x02 > 0, b & 0x01 > 0, false, i == len(bytes)-1)
		im.(*image.RGBA).Set(x, y, c)

		x, y, err = colorIterator.Next()
		if err != nil {
			return err
		}

	}

	err = bmp.Encode(out, im)
	return
}

func (e BMPSteganographyEngine) Reveal (in io.Reader, out io.Writer) (err error) {
	im, err := bmp.Decode(in)
	if err != nil {
		return
	}

	var x, y int
	colorIterator := colorIterator{
		0,
		0,
		im.Bounds().Max.X,
		im.Bounds().Max.Y,
	}

	var bytes []byte

	for {
		var by byte
		by = 0x00

		r, g, b, _ := im.At(x, y).RGBA()

		if r % 2 == 0 {
			by = by | 0x80
		}

		if g % 2 == 0 {
			by = by | 0x40
		}

		if b % 2 == 0 {
			by = by | 0x20
		}

		x, y, err = colorIterator.Next()
		if err != nil {
			err = nil
			break
		}

		r, g, b, _ = im.At(x, y).RGBA()

		if r % 2 == 0 {
			by = by | 0x10
		}

		if g % 2 == 0 {
			by = by | 0x08
		}

		if b % 2 == 0 {
			by = by | 0x04
		}

		x, y, err = colorIterator.Next()
		if err != nil {
			err = nil
			break
		}

		r, g, b, _ = im.At(x, y).RGBA()

		if r % 2 == 0 {
			by = by | 0x02
		}

		if g % 2 == 0 {
			by = by | 0x01
		}

		bytes = append(bytes, by)

		if b & 0x000F == 0 {
			break
		}

		x, y, err = colorIterator.Next()
		if err != nil {
			err = nil
			break
		}
	}

	out.Write(bytes)

	return
}
