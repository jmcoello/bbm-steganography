#!/bin/bash

go run main.go -base testsrc/white.bmp -message ./mysecret -out ./secretimg.bmp
go run main.go -in secretimg.bmp -out ./recoveredsecret
cat ./recoveredsecret
rm ./secretimg.bmp ./recoveredsecret